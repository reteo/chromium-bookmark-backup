"""backup_chromium_bookmarks.py:

Script to back up chromium bookmarks from the command line or a script for automation purposes."""

import json
import html
import os
import platform


def read_config(config_path):
    config = {}
    with open(config_path, "r") as file:
        for line in file:
            line = line.strip()
            if not line or line.startswith("#"):
                continue  # Skip empty lines and comments
            key, value = line.split("=", 1)
            config[key] = value
    return config


def read_bookmarks_file(file_path):
    """Opens the bookmarks file from the Chromium profile."""
    with open(file_path, "r", encoding="utf-8") as file:
        return json.load(file)


def extract_bookmarks(bookmark_node, depth=0):
    """Recursively follows folder nodes, extracting bookmarks and folders as it finds them."""
    bookmarks_html = ""
    if "type" in bookmark_node:
        if bookmark_node["type"] == "url":
            title, url = map(html.escape, (bookmark_node["name"], bookmark_node["url"]))
            bookmarks_html += f'{" " * depth}<DT><A HREF="{url}">{title}</A>\n'
        elif bookmark_node["type"] == "folder":
            title = html.escape(bookmark_node["name"])
            bookmarks_html += f'{" " * depth}<DT><H3>{title}</H3>\n'
            bookmarks_html += f'{" " * depth}<DL><p>\n'
            for child in bookmark_node["children"]:
                bookmarks_html += extract_bookmarks(child, depth + 2)
            bookmarks_html += f'{" " * depth}</DL><p>\n'
    return bookmarks_html


def bookmarks_to_html(bookmarks_html):
    """Wraps the final html bookmark list in the appropriate tags for an acceptable file."""
    return f"<!DOCTYPE NETSCAPE-Bookmark-file-1>\n<HTML>\n<BODY>\n<DL><p>\n{bookmarks_html}</DL><p>\n</BODY>\n</HTML>"


def save_file(content, file_path):
    """Saves the content to a file on the disk."""
    with open(file_path, "w", encoding="utf-8") as file:
        file.write(content)


def export_chromium_bookmarks_to_html(chromium_bookmarks_path, exported_bookmarks_path):
    """Takes the contents of the Chromium bookmarks JSON and exports them to an HTML file."""

    bookmarks_data = read_bookmarks_file(chromium_bookmarks_path)

    # Process "Bookmarks Bar" and "Other Bookmarks" as separate top-level folders
    bookmarks_bar_html = extract_bookmarks(
        bookmarks_data["roots"]["bookmark_bar"], depth=0
    )
    other_bookmarks_html = extract_bookmarks(bookmarks_data["roots"]["other"], depth=0)

    # Combine the HTML for both top-level folders
    combined_html = bookmarks_bar_html + other_bookmarks_html

    html_content = bookmarks_to_html(combined_html)
    save_file(html_content, exported_bookmarks_path)


def generate_config_file(config_path):
    """Useful if a configuration file is not present; this will ensure that a
    configuration file will be added"""

    default_chromium_bookmarks_path = ""
    system_platform = platform.system()

    if system_platform == "Windows":
        default_chromium_bookmarks_path = os.path.expanduser(
            "~\\AppData\\Local\\Chromium\\User Data\\Default\\Bookmarks"
        )
    elif system_platform == "Linux":
        default_chromium_bookmarks_path = os.path.expanduser(
            "~/.config/chromium/Default/Bookmarks"
        )
    elif system_platform == "Darwin":  # macOS
        default_chromium_bookmarks_path = os.path.expanduser(
            "~/Library/Application Support/Chromium/Default/Bookmarks"
        )

    default_exported_bookmarks_path = os.path.expanduser(
        "~/Documents/exported_bookmarks.html"
    )

    config_content = (
        "# Configuration file for the chromium bookmarks backup script.\n\n"
        "# Directory containing the Chromium Bookmarks\n"
        f"chromium_bookmarks_path={default_chromium_bookmarks_path}\n\n"
        "# Directory for the exported HTML bookmarks file\n"
        f"exported_bookmarks_path={default_exported_bookmarks_path}\n"
    )

    save_file(config_content, config_path)

    print(f"Created default configuration file at {config_path}")


def main():
    config_path = os.path.expanduser("~/.config/chromium-bookmarks-backup.conf")

    if not os.path.exists(config_path):
        generate_config_file(config_path)

    config = read_config(config_path)
    chromium_bookmarks_path = config.get("chromium_bookmarks_path")
    exported_bookmarks_path = config.get("exported_bookmarks_path")

    if not chromium_bookmarks_path or not exported_bookmarks_path:
        raise ValueError(
            "Configuration file must include both 'chromium_bookmarks_path' and 'exported_bookmarks_path'."
        )

    export_chromium_bookmarks_to_html(chromium_bookmarks_path, exported_bookmarks_path)


if __name__ == "__main__":
    main()
