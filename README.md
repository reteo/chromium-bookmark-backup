# Chromium Bookmark Backup

This is a simple script to back up Chromium bookmarks in a way that
can be automated from the command line, without cloud shenanigans.
What this does is directly create an HTML bookmarks file using the
Bookmarks JSON file within your Chromium profile.

It's a work in progress; bookmarks (`bookmarks bar` and `other
bookmarks`) are imported as sub-folders of the bookmarks bar, as
opposed to being directly placed in the bookmarks bar and "other
bookmarks".  Once you import them, you will need to manually move the
bookmarks to their desired location, but otherwise, they are backed up
completely, and nothing is lost.

The script is run using python:
* `python chromium_bookmark_backup.py`

When you run the script for the first time, it will create the
`chromium-bookmarks-backup.conf` file in your configuration directory,
where you can edit the file to determine where the original Chromium
Bookmarks file, and your desired HTML output file, are placed.  By
default, it will search in the `Default` profile for Chromium, and
place the HTML file in your Documents folder.

The locations are as follows (The `~` means your user folder) :
* **Windows**: `~\AppData\Local\Chromium\User Data\Default\Bookmarks`
* **MacOS**: `~/Library/Application Support/Chromium/Default/Bookmarks`
* **Linux**: `~/.config/chromium/Default/Bookmarks`

After that initial run, and any changes you make to the configuration,
you can run the script without any additional arguments.
